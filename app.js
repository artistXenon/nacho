const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const vue =  require(`./routes/index`);
const { debug, manage } = require(`./modules/app.cfg.json`);

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.disable('x-powered-by');

app.use((q,s,n) => {
    if(q.headers.host == `manage.nacho.moe` || debug) {
        if(manage) vue(q,s,n);
        else s.send(`공사중입니다. x_X`);
    }
    else n();
});

app.get('/', (q,s,n) => {
    let redirect = null;
    console.log(q.headers.host);
    switch(q.headers.host) {
        case 'twitch.nacho.moe':
            redirect = `https://www.twitch.tv/nannanacho`;
            break;
        case 'youtube.nacho.moe':
            redirect = `https://www.youtube.com/channel/UCqjUPx-UAFHVvthd71bY7_g`;
            break;
        case 'donate.nacho.moe':
            redirect = `https://twip.kr/donate/nannanacho`;
            break;
        case 'kakao.nacho.moe':
            redirect = `https://open.kakao.com/o/sGOqc71`;
            break;
        case 'tgd.nacho.moe':
            redirect = `https://tgd.kr/kjh203520`;
            break;
        case 'discord.nacho.moe':
            redirect = `https://discord.gg/gQJsME`;
            break;
        default:
            s.sendFile(path.join(__dirname, `public/index.html`));
    }
    if(redirect) {
        s.redirect(redirect);
    }
});

app.get('/:app', (req, res, next) => {
    const app = req.params.app;
    if(app == 'mini') res.redirect('https://toon.at/donate/636731303293026499');
    else if(app == 'real') res.redirect('https://fancim.me/bbs/profile.php?mb_id=kjh203520');
    else next();
});


app.use((q,s,n) => {
    s.sendStatus(404);
});

module.exports = app;
