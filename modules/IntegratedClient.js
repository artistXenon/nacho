/*
const chatListener = require(`./private_modules/commands`);

const cl = new chatListener();

cl.use((obj, client, skip) => {
    if(obj.common.self) return skip(); 
});
*/
//한글 정규 표현식 [ㄱ-ㅎ|ㅏ-ㅣ|가-힣]
const { readRecords } = require(`./data`);

module.exports = async (obj, client, skip) => {
    if(obj.common.self) return skip();
    const rules = await readRecords(`rules`, [], _=>_.active&&_.type==`active`);
    console.log(rules[0]);
    return skip();
};