const fs = require("fs");
const path = require(`path`);
const tmi = require("tmi.js");

const youtube = require("./private_modules/youtube");
const cl = require(`./IntegratedClient`);

const config = JSON.parse(fs.readFileSync("app.cfg.json"));

const twitchClient = new tmi.Client({//https://api.twitch.tv/kraken/streams/
    identity: {
        username: config.username,
        password: config.password
    },
    channels: [`wltn4765`],
    options: {
        debug: false
    },
    connection: {
        reconnect: true,
        secure: true
    }
});


cl.setClient({twitch: twitchClient});
/*
const youtubeClient = new youtube({
    config: require(`./client_secret.json`),
    tokenPath: path.resolve(`./token2.json`),
    debug: true,
    channels: [
        `UCQfISxq5legTqOt1h1us6lg`
    ]
});
*/
twitchClient.connect();
//youtubeClient.connect();

            /*
process.on("SIGINT", () => {
    client.disconnect()
        .then(() => {
            fs.writeFileSync(
                config.commands,
                JSON.stringify(commands.sort((a, b) => {
                    if (a.name < b.name) { return -1; }
                    else if (a.name > b.name) { return 1; }
                    else { return 0; }
                }), null, 4) + "\n",
                () => {
                    logger.warn(`Could not save commands to ${config.commands}!`);
                }
            );
            process.exit(0);
        });
});

*/
twitchClient.on("chat", cl.listen.twitch);
//youtubeClient.onChat(cl.client.youtube);
