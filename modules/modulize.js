const path = require(`path`);
const tmi = require("tmi.js");

const youtube = require("./private_modules/youtube");
const chatListener = require(`./private_modules/commands`);

const cl = new chatListener();

//한글 정규 표현식 [ㄱ-ㅎ|ㅏ-ㅣ|가-힣]


const config = require("./app.cfg.json");


const twitchClient = new tmi.Client({//https://api.twitch.tv/kraken/streams/
    identity: {
        username: config.twitch.username,
        password: config.twitch.password
    },
    channels: config.channels.map(_=>_.twitch),
    options: {
        debug: config.debug
    },
    connection: {
        reconnect: true,
        secure: true
    }
});

const youtubeClient = new youtube({
    config: config.youtube.OAuth,
    tokenPath: path.resolve(`./token.json`),
    debug: config.debug,
    reconnect: true,
    channels: config.channels.map(_=>_.youtube)
});

twitchClient.on(`chat`, cl.listen.twitch);
youtubeClient.onChat(cl.listen.youtube);

cl.setClient({twitch: twitchClient, youtube: youtubeClient});
cl.setChannels(config.channels);

cl.use((obj,a,b,c) => {
    if(config.debug)
    console.log(
        `[${(new Date(obj.common.submitTime)).toLocaleTimeString()}] ` + 
        `<${obj.common.channelName}@${obj.common.platform}> (${obj.common.displayName}): ${obj.common.message}`
    );
});



module.exports = cl;