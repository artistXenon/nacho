const { readRecords } = require(`./data`);


function intervalManager(chatListener, channels) {
    this.chatListener = chatListener;
    this.channels = channels;
    this.rules = [];
    this.intervals = [];    
}

intervalManager.prototype.resetInterval = async function() {
    const connectedClients = this.chatListener.getClient();
    this.clearIntervals();
    this.rules = await readRecords(`rules`, [], _=>
    _.active && _.type==`passive` &&
    ((!!connectedClients.twitch.ws && _.platform&1) ||
    (!!connectedClients.youtube.channelObjects && _.platform&2)));
    
    for(let i=0;i<this.rules.length;i++) {
        this.intervals[i] = setInterval(()=> {
            if(this.rules[i].platform&1)
                connectedClients.twitch.say(this.channels.twitch, this.rules[i].detail.action);
            if(this.rules[i].platform&2)
                connectedClients.youtube.say(this.channels.youtube, this.rules[i].detail.action);
        }, this.rules[i].detail.period*1000);
    }
    /*
    
    */
}

intervalManager.prototype.clearIntervals = function() {
    this.intervals.forEach(clearInterval);
}

module.exports = intervalManager;