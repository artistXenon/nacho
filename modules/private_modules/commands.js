function chatListener() {
    const listener = this;
    this.commandList = [];
    this.channels = [];
    this.client = {
        twitch: null,
        youtube: null
    }
    this.listen = {
        twitch : (targetChannel, context, rawMessage, self) => {
            let breakFlag = false;
            const breaker = () => breakFlag=true;
            this.commandList.forEach(async command => {
                if(breakFlag) return;
                const { includes, startsWith, endsWith, matches, regex, action } = command;

                const object = function(target, ctxt, msg, slf) {
                    let stat = 0;
                    if(ctxt.badges ? ctxt.badges.partner : null) stat|=1;
                    if(ctxt.mod) stat|=2;
                    if(ctxt.badges ? ctxt.badges.broadcaster : null) stat|=4;
                    return {
                        common: {
                            self: slf,
                            channelName: target,
                            message: msg,
                            messageId: ctxt.id,
                            displayName: ctxt["display-name"],
                            submitTime: !isNaN(ctxt["tmi-sent-ts"]) ? Number(ctxt["tmi-sent-ts"]) : Date.now(),
                            userState: stat,
                            platform: `twitch`
                        },
                        twitch: {
                            target,
                            context: ctxt,
                            msg,
                            self: slf
                        }
                    };
                }(targetChannel, context, rawMessage, self);

                if(!includes && !startsWith && !endsWith && !matches && !regex) 
                    return await action(object, listener.client.twitch, breaker, listener.channels.find(_=>_.twitch==targetChannel));
                
                const trimmedCommand = rawMessage.trim();
                if(
                    trimmedCommand.startsWith(startsWith) || 
                    trimmedCommand.endsWith(endsWith) || 
                    trimmedCommand.includes(includes) || 
                    trimmedCommand === matches ||
                    regex.test(trimmedCommand)
                )
                    return await action(object, listener.client.twitch, breaker, listener.channels.find(_=>_.twitch==targetChannel));
                
            });
        },
        youtube: (channel, id, snippet, authorDetails) => {
            let breakFlag = false;
            const breaker = () => breakFlag=true;
            this.commandList.forEach(async command => {
                if(breakFlag) return;
                const { includes, startsWith, endsWith, matches, regex, action } = command;

                const object = function(chn, id, snippet, authorDetails) {
                    let stat = 0;
                    if(authorDetails.isVerified) stat|=1;
                    if(authorDetails.isChatModerator) stat|=2;
                    if(authorDetails.isChatOwner) stat|=4;
                    return {
                        common: {
                            self: listener.client.youtube.self === authorDetails.channelId,
                            channelName: chn.info.snippet.channelTitle,
                            message: snippet.textMessageDetails ? snippet.textMessageDetails.messageText : snippet.superChatDetails ? snippet.superChatDetails.userComment : `unknown format`,
                            messageId: id,
                            displayName: authorDetails.displayName,
                            submitTime: Date.parse(snippet.publishedAt),
                            userState: stat,
                            platform: `youtube`
                        },
                        youtube: {
                            channel: chn,
                            id, 
                            snippet, 
                            authorDetails
                        }
                    }
                }(channel, id, snippet, authorDetails);

                if(!includes && !startsWith && !endsWith && !matches && !regex) {
                    return await action(object, listener.client.youtube, breaker, listener.channels.find(_=>_.youtube==channel.info.snippet.channelId));
                }
                
                const trimmedCommand = snippet.textMessageDetails.messageText.trim();
                if(
                    trimmedCommand.startsWith(startsWith) || 
                    trimmedCommand.endsWith(endsWith) || 
                    trimmedCommand.includes(includes) ||
                    trimmedCommand === matches ||
                    regex.test(trimmedCommand)
                )
                    return await action(object, listener.client.youtube, breaker, listener.channels.find(_=>_.youtube==channel.info.snippet.channelId));
            });

        }
    }
};

/**
 * @param {string} includes 명령 이름
 * @param {function} action 수행할 작업
 */
chatListener.prototype.includes = function(includes, action) {
    if(typeof includes === 'string' && typeof action === 'function') this.commandList.push({includes, action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {string} startsWith 명령 이름
 * @param {function} action 수행할 작업
 */
chatListener.prototype.startsWith = function(startsWith, action) {
    if(typeof startsWith === 'string' && typeof action === 'function') this.commandList.push({startsWith, action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {string} endsWith 명령 이름
 * @param {function} action 수행할 작업
 */
chatListener.prototype.endsWith = function(endsWith, action) {
    if(typeof endsWith === 'string' && typeof action === 'function') this.commandList.push({endsWith, action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {string} matches 명령 이름
 * @param {function} action 수행할 작업
 */
chatListener.prototype.matches = function(matches, action) {
    if(typeof matches === 'string' && typeof action === 'function') this.commandList.push({matches, action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {RegExp} regex 명령 이름
 * @param {function} action 수행할 작업
 */
chatListener.prototype.regex = function(regex, action) {
    if(regex instanceof RegExp && typeof action === 'function') this.commandList.push({regex, action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {function} action 수행할 작업
 */
chatListener.prototype.use = function(action) {
    if(typeof action === 'function') this.commandList.push({action});
    else {
        const e = new Error(`invalid parameter`);
        throw e.code = `IVP`;
    }
};

/**
 * @param {Object} client 트위치/유튜브 클라이언트
 */
chatListener.prototype.setClient = function(client) {
    this.client = client;
};
chatListener.prototype.setChannels = function(channels) {
    this.channels = channels;
}

/**
 * @param {string} client 트위치/유튜브 클라이언트
 */
chatListener.prototype.getClient = function(client) {
    return client ? this.client[client] : this.client;
};



module.exports = chatListener;


/**공용 함수
 * 
 * string, 
 * function action(message, ) {
 * 
 * }
 */