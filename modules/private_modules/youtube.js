const fs = require('fs');
const path = require('path');
const readline = require('readline');
const { google: { auth: { OAuth2 } }, google } = require('googleapis');//.google.auth.OAuth2;


function defaultLogger(message, timemillisec) {
    const time = new Date(timemillisec ? timemillisec : Date.now());
    console.log(`[${time.getHours()}:${time.getMinutes()}] info: %s`, String(message));
}

/**
 * @param {Object} configuration 
*/
function youtubeClient(configuration) {
    const client = this;
    this.reconnect = configuration.reconnect;
    this.debug = configuration.debug;
    this.channels = configuration.channels;
    this.channelObjects;
    this.config = configuration.config;
    this.tokenPath = configuration.tokenPath;
    this.token = fs.existsSync(configuration.tokenPath) ? require(configuration.tokenPath) : null;
    this.oauth2Client;
    this.service = google.youtube('v3');
    this.liveChatId;
    this.self;


    this.listener = [];
    if (this.debug) this.listener[0] = (channel, id, snippet, authorDetails) => {
        defaultLogger(`[$${channel.info.snippet.channelTitle}] <${authorDetails.displayName}>: ${snippet.textMessageDetails ? snippet.textMessageDetails.messageText : snippet.superChatDetails ? snippet.superChatDetails.userComment : snippet}`, Date.parse(snippet.publishedAt));
    }

    // Load client secrets from a local file.
    /**
     * Create an OAuth2 client with the given credentials.
     */
    (function () {
        if(client.debug)
            defaultLogger(`Checking Google OAuth info..`);
            client.oauth2Client = new OAuth2(
                client.config.installed.client_id,
                client.config.installed.client_secret,
                client.config.installed.redirect_uris[0]
            );

        if (client.token)
            client.oauth2Client.credentials = client.token;
        else
            /**
             * Get and store new token after prompting for user authorization.
             */
            (function () {
                defaultLogger(
                    'Authorize this app by visiting this url: \n' +
                    client.oauth2Client.generateAuthUrl({
                        access_type: 'offline',
                        scope: ['https://www.googleapis.com/auth/youtube']
                    })
                );
                const rl = readline.createInterface({
                    input: process.stdin,
                    output: process.stdout
                });
                rl.question('Enter the code from that page here: \n', function (code) {
                    rl.close();
                    client.oauth2Client.getToken(code, function (err, token) {
                        if (err) {
                            const e = new Error(`Error while trying to retrieve access token: ${err}`);
                            throw e.code = `IVT`;
                        }
                        client.token = token;
                        client.oauth2Client.credentials = token;
                        /**
                         * Store token to disk be used in later program executions.
                         */
                        if (client.tokenPath)
                            (function (token) {
                                try {
                                    fs.mkdirSync(path.resolve(client.tokenPath, '..'));
                                } catch (err) {
                                    if (err.code != 'EEXIST') throw err;
                                }
                                fs.writeFile(client.tokenPath, JSON.stringify(token), err => {
                                    if (err) throw err;
                                    if(client.debug) defaultLogger('Token stored to: ' + client.tokenPath);
                                });
                            })(token);
                    });
                });
            })();
        if(client.debug) defaultLogger(`Ready to connect to YouTube.`);
    })();
};

youtubeClient.prototype.isReady = function () {
    return !!this.token;
}

youtubeClient.prototype.findChannel = function (channelId) {
    const ch = this.channelObjects ? this.channelObjects.find(channel =>
        channel.info.snippet.channelId === channelId
    ) : null;
    if (!ch) {
        const e = new Error(`we are not listening to such channel`);
        e.code = `NOSCH`;
        throw e;
    }
    return ch;
}


youtubeClient.prototype.connect = async function() {
    this.reconnect = true;
    this.con();
}

youtubeClient.prototype.getChannelLive = async function(channelId) {
    return await this.service.search.list({
        auth: this.oauth2Client,
        part: `id,snippet`,
        channelId,
        eventType: `live`,
        type: `video`
    })
}

youtubeClient.prototype.getLiveVideo = async function(videoId) {
    return await this.service.videos.list({
        auth: this.oauth2Client,
        part: 'liveStreamingDetails,snippet',
        id: videoId
    })
}

youtubeClient.prototype.con = async function () {
    if (!this.token) console.log('\x1b[33m%s\x1b[0m', `Warning: Connection will be delayed until the client is authorized`);
    while (!this.token) await new Promise(r => setTimeout(r, 2000));    
    if(this.debug) defaultLogger(`Connecting to live streams..`);
    this.self = (await this.service.channels.list({
        auth: this.oauth2Client,
        part: `id`,
        mine: true
    })).data.items[0].id;
    this.channels.forEach(async (channel, index, array) => {
        
        let search;
        do {
            search = (await this.getChannelLive(typeof channel === 'string' ? channel : channel.info.snippet.channelId)).data.items[0];
            if(search) break;
            if(!this.reconnect) throw new Error(`The specified channel is not live at the momment`);
            if(this.debug) defaultLogger(`Channel not live, retrying to connect..`);
            await new Promise(r => setTimeout(r, 1e5));//300000));
        } while(1)


        if(this.debug) defaultLogger(`Channel located: <${search.snippet.channelTitle}>..`);
        this.channelObjects = [];
        this.channelObjects[index] = {
            info: search
        };
        let live;
        do {
            live = (await this.getLiveVideo(this.channelObjects[index].info.id.videoId)).data.items[0];
            if(live && live.liveStreamingDetails) break;
            if(!this.reconnect) throw new Error(`The specified video is not live at the momment`);
            await new Promise(r => setTimeout(r, 300000));
        } while(0)

        if(this.debug) defaultLogger(`Joining live stream.. : ${search.snippet.title}`);
        this.channelObjects[index].live = live.liveStreamingDetails;
        this.channelObjects[index].chatListening = setInterval(() => {
            const param = {
                auth: this.oauth2Client,
                part: 'authorDetails,snippet',
                liveChatId: this.channelObjects[index].live.activeLiveChatId
            };
            if (this.channelObjects[index].pageToken) param.pageToken = this.channelObjects[index].pageToken;
            this.service.liveChatMessages.list(param, (err, response) => {
                if (err) throw err;
                const res = response.data;
                this.channelObjects[index].pageToken = res.nextPageToken;
                res.items.forEach(({ id, snippet, authorDetails }) =>
                    this.listener.forEach(async routes =>
                        await routes(this.channelObjects[index], id, snippet, authorDetails)
                    )
                );
            });
        }, 5000);
        if(this.debug) defaultLogger(`Connected to live stream. : ${search.snippet.title}`);
        /*
        this.channelObjects[index].liveListening = setInterval(() => {
            this.service.liveChatMessages.list(param, (err, response) => {
                if (err) throw err;
                const res = response.data;
                this.channelObjects[index].pageToken = res.nextPageToken;
                res.items.forEach(({ id, snippet, authorDetails }) =>
                    this.listener.forEach(async routes =>
                        await routes(this.channelObjects[index], id, snippet, authorDetails)
                    )
                );
        });
        */
    });
};
youtubeClient.prototype.disconnect = async function () {
    this.reconnect = false;
    this.channelObjects.forEach(channel => {
        clearInterval(channel.chatListening);
//        clearInterval(channel.liveListening);
    });
    this.channelObjects = null;
    if(this.debug) defaultLogger(`Disconnected`);
}

/**
 * @param {function} routes 수행할 작업
 */
youtubeClient.prototype.onChat = function (routes) {
    if (typeof routes === 'function') this.listener.push(routes);
    else {
        throw new TypeError(`youtubeClient.onChat only accepts functions as router`);
    }
};


/**
 * @param {string} channelId 전송 대상 채널
 * @param {string} messageText 전송할 메시지
 */
youtubeClient.prototype.say = async function(channelId, messageText) {
    const ch = this.findChannel(channelId);
    return await this.service.liveChatMessages.insert({
        auth: this.oauth2Client,
        part: 'snippet',
        resource: {
            snippet: {
                liveChatId: ch.live.activeLiveChatId,
                type: "textMessageEvent",
                textMessageDetails: {
                    messageText
                }
            }
        }
    });
};


youtubeClient.prototype.ban = async function(channelId, userId, duration) {
    const ch = this.findChannel(channelId);
    const snippet = {
        liveChatId: ch.live.activeLiveChatId,
        bannedUserDetails: {
            channelId: userId
        }
    };
    if(duration === -1) snippet.type = `permanent`;
    else snippet.type = `temporary`,snippet.banDurationSeconds = duration;
    
    return await this.service.liveChatBans.insert({
        auth: this.oauth2Client,
        part: 'snippet',
        resource: {
            snippet
        }
    });
};

/**
 * @param {string} messageId 삭제할 채팅 아이디
 */
youtubeClient.prototype.deleteMessage = async function (messageId) {
    return await this.service.liveChatMessages.delete({
        auth: this.oauth2Client,
        id: messageId
    });
};

/**
 * @param {string} channelId 삭제할 채팅 아이디
 *
youtubeClient.prototype.mods = async function (channelId) {
    const ch = this.findChannel(channelId);
    return await this.service.liveChatModerators.list({
        auth: this.oauth2Client,
        liveChatId: ch.live.activeLiveChatId,
        part: `id,snippet`
    });
};
*/

//youtubeClient.prototype.unban = async function() {}



module.exports = youtubeClient;



/** SUPERCHAT RESPONSE
    {
        type: 'superChatEvent',
        liveChatId: 'EiEKGFVDZmRMRG5HY1JNM29rRU90b2NXTkh1URIFL2xpdmUqJwoYVUNmZExEbkdjUk0zb2tFT3RvY1dOSHVREgtubU9YR2tYekpySQ',
        authorChannelId: 'UCenKGl5SOQh3s4H3VSG7jOw',
        publishedAt: '2020-02-21T04:09:39.992Z',
        hasDisplayContent: true,
        displayMessage: '₩2,000 from 일단하프: "1대1신청 합니다~!!"',
        superChatDetails: {
            amountMicros: '2000000000',
            currency: 'KRW',
            amountDisplayString: '₩2,000',
            userComment: '1대1신청 합니다~!!',
            tier: 2
        }
    }
 */