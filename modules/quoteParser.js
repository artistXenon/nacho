const axios = require(`axios`);
const { parse: HTMLparse } = require(`node-html-parser`);

const { readRecords, addRecord, removeRecords } = require(`../modules/data`);

const PREFIX = `$`;

const characterInfo = [];

const channelInfo = {
    youtube: {
        liveTitle: ``,
        upSince: 0,
        update: 0
    },
    twitch: {        
        liveTitle: ``,
        upSince: 0,
        update: 0
    }
};

async function getGG(name) {
    let synced = false;
    while(!synced){
        const _ = await axios.get(`https://maple.gg/u/${encodeURIComponent(name)}/sync`);
        
        if(_.data.error) {
            return `캐릭터를 찾을 수 없습니다.`
        }
        synced = _.data.done;
        await new Promise(r => setTimeout(r, 500));
    }
    const res = await axios.get(`https://maple.gg/u/${encodeURIComponent(name)}`);
    const p = HTMLparse(res.data);

    const info1 = p.querySelector(`#user-profile`);
    const srvNnck = info1.querySelectorAll(`h3 *`);
    const lvlNjob = info1.querySelectorAll(`li.user-summary-item`);    
    const info2 = p.querySelectorAll(`.box.user-summary-box`);

    const nwch = {
        name: srvNnck[1].text,
        level: lvlNjob[0].text,
        server: srvNnck[0].getAttribute(`alt`),
        guild: info1.querySelector(`.user-additional div`).childNodes[3].text,
        job: lvlNjob[1].text,
        union: info2[2].childNodes[3].getAttribute(`class`) == `user-summary-no-data` ? `알 수 없음` : info2[2].childNodes[3].querySelector(`span`).text,
        dojo: info2[0].childNodes[3].getAttribute(`class`) == `user-summary-no-data` ? `알 수 없음` : info2[0].childNodes[3].querySelector(`h1`).text.replace(/[\s\v\t]+/,''),
        update: Date.now()
    };
    characterInfo.push(nwch);
    return nwch;
}

async function getLive(platform) {

}


const varList = {
    유니온: async (_,mainChar) => {
        const c = characterInfo.find(char => char.name.toLowerCase()==mainChar.toLowerCase());
        if(c) {
            if(Date.now() - c.update < 36e5) return c.union;        
            characterInfo.splice(characterInfo.indexOf(c));    
        }
        return (await getGG(mainChar)).union;

    },
    캐릭터정보: async (_,charName) => {
        let c = characterInfo.find(char => char.name.toLowerCase()==charName.toLowerCase());
        if(c && Date.now() - c.update > 36e5)      
            characterInfo.splice(characterInfo.indexOf(c));    
        
        c = await getGG(charName);
        return (({name,level,server,guild,job,union,dojo})=>`${server}〈${guild}〉${level} ${job}「${name}」무릉${dojo} 유니온${union}`)(c);
    },
    랜덤: (_,max) => {
        if(isNaN(max)) {
            const rd = max.split(`,`);
            return rd[Math.ceil(Math.random()*rd.length)]
        }
        else return Math.ceil(Math.random()*max);
    },
    닉: context => context.common.displayName,/*
    방제: context => {},
    업타임: context => {},
    시청자수: context => {},*/

};

function findAllIndex(str, key, offset, max) {
    if(offset == null) offset = 0;
    if(max == null) max = str.length;
    const count = [];
    let pos = str.indexOf(key, offset); 

    while (pos !== -1 && count.length < max) {
        count.push(pos);
        pos = str.indexOf(key, pos + 1);
    }
    return count;
}


const quote = async (q, context, parameter1) => {
    if(!q.includes(PREFIX)) return q;
    let beforeString = q;
    const keyList = varList;
    if(parameter1) keyList[`param1`] = _=>parameter1;

    const regex = new RegExp(`\\${PREFIX}(${Object.keys(keyList).join(`|`)})(\\[([^\\[\\]]*)\\])?`,`g`);
    //`(\\${PREFIX}(${Object.keys(keyList).join(`|`)})(\\[[^\\[\\]]*\\])?)`, 'g');
    let m;
    
    do {
        m = regex.exec(beforeString);
        if (m) {
            const k = m[1];
            const func = keyList[k];
            const result = await func(context, m[3]);
            beforeString = beforeString.replace(m[0], result);      
        } 
    } while (m);
    return beforeString;
}

module.exports = {
    quote,
    varList
}
/**
 * 
 * 
        const kValue = keyList
        key = PREFIX + key;
        const allIdx = findAllIndex(beforeString, key);
        const realIdx = [];
        const splitString = beforeString.split(key);
        allIdx.forEach(idx => {
            if(beforeString[idx-1] != `\\`) realIdx.push(idx);
        });
        let afterString = allIdx[0]==0 ? `` : splitString.shift();
        while(allIdx.length !=0){
            if(allIdx.shift() == realIdx[0]) afterString += ``;
            afterString += splitString.shift();
        }

        
    Object.keys(keyList).forEach(key => {
        const varIdx = findAllIndex(beforeString, `${PREFIX}${key}`);
        const kValList = [];
        varIdx.forEach(idx => {
            const kValIdx = [idx+key.length+2];
            let kVal = null;
            if(beforeString[kValIdx[0]-1] == `[` &&
                (kValIdx[1] = beforeString.indexOf(`]`, kValIdx[0]))){
                    kVal = beforeString.slice(...kValIdx)
            }
            kValList.push(kVal);
        });
    });
 */