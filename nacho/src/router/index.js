import Vue from 'vue'
import VueRouter from 'vue-router'
import PageNotFound from '../views/404.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: function () {
      return import('../views/Home.vue')
    }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import('../views/About.vue')
    }
  },
  {
    path: '/active',
    name: 'Active',
    component: function () {
      return import('../views/Active.vue')
    }
  },
  {
    path: '/passive',
    name: 'Passive',
    component: function () {
      return import('../views/Passive.vue')
    }
  },
  {
    path: '/log',
    name: 'Log',
    component: function () {
      return import('../views/Log.vue')
    }
  },
  { path: "*", component: PageNotFound }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
