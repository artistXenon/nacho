import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexStorage = new VuexPersist({
  key: 'moe.nacho.vuex',
//  asyncStorage: true,
  storage: window.localStorage,
})

export default new Vuex.Store({
  plugins: [vuexStorage.plugin],
  state: {
    ssid: null
  },
  getters: {
    getSession(state, getter) {
      return state.ssid;
    }
  },
  mutations: {
    setSession(state, session) {
      state.ssid = session;
    },
    resetSession(state) {
      state.ssid = null;
    }
  },
  actions: {
    resetSession(context) {
      context.commit(`resetSession`);
      $router.push({path: `/`});
    }
  }
})
