const path = require('path');
const express = require('express');
const uuid = require('uuid/v4');

const DBM = require('../modules/data');
const chatListener = require(`../modules/modulize`);
const { quote, varList } = require(`../modules/quoteParser`);
const intervalManager = require(`../modules/passive`);

const im = new intervalManager(chatListener, chatListener.channels[0]);

im.resetInterval();

function csParser(string, cs) {
  return cs ? string : string.toLowerCase();
}


chatListener.use(async (obj, client, skip, channelSet) => {
  if(obj.common.self) return skip();
  const rules = await DBM.readRecords(`rules`, [], _=>_.active&&_.type==`active`&&Date.now()-_.lastCall>_.detail.cool*1000);
  const triggered = [];
  rules.forEach(async rule => {
    let trg = false;
    const pms = rule.detail.permission.reduce((a,b)=>Number(a)+Number(b));
    if(!(obj.common.userState&4) && 
      !(obj.common.userState&2) && 
      !(!(obj.common.userState&6) && pms&1))
        return;

    if(rule.platform&1 && obj.common.platform == `twitch`) {//IF FROM TWITCH
      const cs = rule.detail.condition.caseSensitive;
      const matcher = rule.detail.condition.type&2 ? [csParser(obj.common.displayName, cs), csParser(obj.twitch.context.username, cs)] : [csParser(obj.common.message, cs)];
      if(rule.detail.condition.type&1 ? 
        !matcher.some(_=>_.includes(csParser(rule.detail.condition.text, cs))) :
        !matcher.some(_=>_==csParser(rule.detail.condition.text, cs))) 
          return;
      if(rule.detail.action.type==0){
        if(rule.detail.action.chat2&1){
          client.say(channelSet.twitch, await quote(rule.detail.action.text, obj));
          trg=true;
        }
        if(rule.detail.action.chat2&2){
          chatListener.getClient(`youtube`)
            .say(channelSet.youtube, await quote(rule.detail.action.text, obj));
          trg=true;
        }
      }
      else if(obj.common.userState&6) return;
      switch(rule.detail.action.type) {
        case '1':
          client.deletemessage(channelSet.twitch, obj.common.messageId);
          trg=true;
          break;
        case '2':
          client.timeout(
            channelSet.twitch, 
            obj.twitch.context.username,
            rule.detail.action.timeout,
            `봇에 의한 채팅 금지 입니다> 일반 사용자의 ${rule.detail.condition.type&2 ? `닉네임`: `채팅`}이 ${rule.detail.condition.caseSensitive ? `대소문자 구별 ` : ``}` +
            `다음 문구${rule.detail.condition.type&1 ? `를 포함` : `와 일치`}: "${rule.detail.condition.text}"`
            );
          trg=true;
          break;
        case '3':
          client.ban(
            channelSet.twitch,
            obj.twitch.context.username,
            `봇에 의한 추방 입니다> 일반 사용자의 ${rule.detail.condition.type&2 ? `닉네임`: `채팅`}이 ${rule.detail.condition.caseSensitive ? `대소문자 구별 ` : ``}` +
            `다음 문구${rule.detail.condition.type&1 ? `를 포함` : `와 일치`}: "${rule.detail.condition.text}"`
          );
          trg=true;
          break;
        default:return;
      }      
    }
    else if(rule.platform&2 && obj.common.platform == `youtube`) {//IF FROM YOUTUBE
      const cs = rule.detail.condition.caseSensitive;
      const matcher = rule.detail.condition.type&2 ? [csParser(obj.common.displayName, cs)] : [csParser(obj.common.message, cs)];
      if(rule.detail.condition.type&1 ? 
          !matcher.some(_=>_.includes(csParser(rule.detail.condition.text, cs))) :
          !matcher.some(_=>_==csParser(rule.detail.condition.text, cs))) 
           return;
      if(obj.youtube.snippet.type==`superChatEvent` && !rule.detail.superchat) return;
      if(rule.detail.action.type==0){
        if(rule.detail.action.chat2&1){
          client.say(channelSet.youtube, await quote(rule.detail.action.text, obj));
          trg=true;
        }
        if(rule.detail.action.chat2&2){
          chatListener.getClient(`twitch`)
            .say(channelSet.twitch, await quote(rule.detail.action.text, obj));
          trg=true;
        }
      }
      else if(obj.common.userState&6) return;
      switch(rule.detail.action.type) {
        case '1':
          client.deleteMessage(obj.common.messageId);
          trg=true;
          break;
        case '2':
          client.ban(
            channelSet.youtube,
            obj.youtube.authorDetails.channelId,
            rule.detail.action.ban.permanent ? -1 : rule.detail.action.ban.temporary*60);
          trg=true;
          break;
        default:return;
      }   
    }    
    if(trg) triggered.push(rule.id);
  });
  await DBM.updateRecords(`rules`,_=>triggered.some(__=>__==_.id), {lastCall: Date.now()});////////////////////////////////////
});

const router = express.Router();



async function isNacho(ssid) {
  const uuid = (await DBM.readRecords(`keytable`, [`value`], row=>Date.now() - row.validate < 86400*30, null, 1))[0];
  return uuid ? uuid.value === ssid : false;
}




/* GET home page. */
router.get('/api', function(req, res, next) {
  res.send(`bye bye`);
});

router.post('/api/verify', async (req,res) => {
  if(req.body.key == '이걸 보러오는 개발자가 있을 줄 알았지렁') {
    await DBM.removeRecords(`keytable`);
    const id = uuid();
    await DBM.addRecord(`keytable`, [{validate: Date.now(), value: id}]);
    const clients = chatListener.getClient();
    return res.json({ssid: id, status: {youtube: !!clients.youtube.channelObjects, twitch: !!clients.twitch.ws}});
  }
  else res.status(401).json({err: '비밀번호가 일치하지 않습니다.'});
});

router.get(`/api/about`, async (req,res) => {
  const whole = await DBM.readRecords(`rules`, ["active","detail","id","platform","type"], _=>_.public, (a,b)=>a<b?-1:a==b?0:1);
  const clients = chatListener.getClient();
  res.json({
    conditions: {
      active: whole.filter(_=>_.type==`active`),
      passive: whole.filter(_=>_.type==`passive`)
    },
    connected: {
      youtube: !!clients.youtube.channelObjects, 
      twitch: !!clients.twitch.ws
    }
  });
});

router.post(`/api/home`, (req, res) => {
  if(!isNacho(req.body.ssid)) return res.status(401);
  const clients = chatListener.getClient();
  res.json({
    status:{
      youtube: !!clients.youtube.channelObjects, 
      twitch: !!clients.twitch.ws
    }
  });
});

router.post('/api/join/:platform/:join', async (req, res) => {  
  if(!isNacho(req.body.ssid)) return res.status(401).json({err: '다시 로그인 하세요.'});
  if(!(/^(join|part)$/.test(req.params.join))) return res.status(400).json({err: '잘못된 요청입니다.'});
  const clients = chatListener.getClient();
  const connected = {
    youtube: !!clients.youtube.channelObjects, 
    twitch: !!clients.twitch.ws
  };
  switch(req.params.platform){
    case 'twitch':
      if(req.params.join == `join` && !connected.twitch){
        clients.twitch.connect();
        connected.twitch = true;
      }
      else if(connected.twitch){
        clients.twitch.disconnect();
        connected.twitch = false;
      }
      break;
    case 'youtube':
      if(req.params.join == `join` && !connected.youtube){
        clients.youtube.connect();
        connected.youtube = true;
      }
      else if(connected.youtube){
        clients.youtube.disconnect();
        connected.youtube = false;
      }
      break;
    case 'all':
      if(req.params.join == `join`) {
        if(!connected.youtube){
          clients.youtube.connect();
          connected.youtube = true;
        }
        if(!connected.twitch){
          clients.twitch.connect();
          connected.twitch = true;
        }
      }
      else {
        if(connected.youtube){
          clients.youtube.disconnect();
          connected.youtube = false;
        }
        if(connected.twitch){
          clients.twitch.disconnect();
          connected.twitch = false;
        }
      }
      break;
    default:
      return res.status(400).json({err: '잘못된 요청입니다.'});
  }
  im.resetInterval();
  res.json({
    status:connected
  });
});

//룰 변경점 반영용 미들웨어
//DBM 개선 필요
router.post('/api/:type/:act', async (req,res) => {
  if(!isNacho(req.body.ssid)) return res.status(401).json({err: '다시 로그인 하세요.'});
  const { type, act } = req.params;
  if(!(/^(active|passive)$/.test(type))) return res.status(400).json({err: '잘못된 요청입니다.'});
  switch(act){
    case 'set':
      await DBM.removeRecords(`rules`, _=>_.type==type);////////////////////////최적화 필요
      await DBM.addRecord(`rules`, req.body.rules);/////////////////////////////
      if(type == `passive`) {
        im.resetInterval();
      }
      return res.json({success: `성공적으로 반영됐습니다.`});
    case 'get':
      return res.json(await DBM.readRecords(`rules`, [], _=>_.type==type));
//      res.json([{id:`29fc03j485u`, type, detail:{condition:`if`, action:`this`, etc:`cool`}}])
    default:
      res.status(400).json({err: '잘못된 요청입니다.'});
  }
});


router.use(require('connect-history-api-fallback')());




router.use(express.static(path.join(__dirname, `../nacho/dist`)));

module.exports = router;
